pulse_web
===========

Flask app for interacting with PULSE on the web.

###Dependencies###
* Ubuntu
* npm
* nodejs
* mongo
* gtdownload
* samtools
* cufflinks
* blastx

###Preprocessing###
(each one has its own resource, with its own API)

1. Upload a manifest XML file of all of the cell lines you want to download.
    * Download using gtdownload
    * Display the download progress
2. Run samtools and cufflinks to generate the transcriptome
    * Display the progress for each downloaded cell line
3. Extract alternative splicing events.
    * Display the progress for the alternative splicing events
4. Blast the output of all alternative splicing events
    * Display the progress for blast
5. Using filtermap.py, filter out any AS events that do not have a good anchor in Uniprot.
6. Generate an index file using generate_index_and_libs_rnaseq.py of the alternative splicing events and their locations in the genome, only of the ones that have good protein anchors.

###Feature Extraction###



### How do I get set up? ###

## Setup Dev Env
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Create Mongo DBs
```
sudo mkdir /data/db  # dev db
sudo mkdir /data/testdb  # testing db
```

## Install front-end dependencies
```
npm install
```

## Run
```
python app.py
```

## Backend Testing
```
nosetests
```

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions