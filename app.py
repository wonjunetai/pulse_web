import os

from flask import send_file

import config

from core import app
from controllers.auth import authapi
from controllers.user import userapi
from models.model import db

app.register_blueprint(authapi)
app.register_blueprint(userapi)
app.config.update(MONGOALCHEMY_SERVER=config.DB_HOST,
                  MONGOALCHEMY_PORT=config.DB_PORT,
                  MONGOALCHEMY_DATABASE=config.DB_NAME,
                  SECRET_KEY=config.SECRET_KEY,
                  DEBUG=config.DEBUG)
db.init_app(app)


@app.route('/')
def index():
    return send_file('client/dist/index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=config.PORT, debug=True)
