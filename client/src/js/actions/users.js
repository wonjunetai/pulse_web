import request from 'superagent';
import auth from 'js/util/auth';

export const RECEIVE_USER = 'RECEIVE_USER';
export const NO_TOKEN_FOUND = 'NO_TOKEN_FOUND';

export function attemptLogin(user, pass, cb) {
  return function(dispatch) {
    return request
        .post('api/token')
        .auth(user, pass)
        .set('Content-Type', 'application/json')
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            auth.logout();
            cb(false);
          } else {
            auth.saveToken(res.body.token);
            dispatch(receiveUser(res.body));
            cb(true);
          }
        })
  }
}

export function register(pass, email, cb) {
  return function(dispatch) {
    var user = {email: email, password: pass};
    return request
        .post('api/users', user)
        .set('Content-Type', 'application/json')
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
              if (err) {
                cb(false, res);
              } else {
                request
                    .post('api/token')
                    .auth(email, pass)
                    .set('Content-Type', 'application/json')
                    .set('X-Requested-With', 'XMLHttpRequest')
                    .end((err, response) => {
                      if (err) {
                        auth.logout();
                        cb(false);
                      } else {
                        auth.saveToken(response.body.token);
                        dispatch(receiveUser(response.body));
                        cb(true);
                      }
                    })
              }
            }
        )
  }
}

export function receiveUser(data) {
  return {
    type: RECEIVE_USER,
    token: data.token,
    user: data.user
  };
}

export function fetchUser(token, cb) {
  return function(dispatch) {
    return request
        .post('api/token')
        .auth(token)
        .set('Content-Type', 'application/json')
        .set('X-Requested-With', 'XMLHttpRequest')
        .end((err, res) => {
          if (err) {
            auth.logout();
            cb(false);
            dispatch(noTokenFound());
          } else {
            auth.saveToken(res.body.token);
            dispatch(receiveUser(res.body));
            cb(true);
          }
        })
  }
}

export function noTokenFound() {
  return {
    type: NO_TOKEN_FOUND
  }
}
