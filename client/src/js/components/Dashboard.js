import React, { Component } from 'react'

// Components
import DashboardHeader from 'js/components/DashboardHeader'
import Preprocess from 'js/components/Preprocess'

class Dashboard extends Component {
  render() {
    return (
    	<div>
        <DashboardHeader />
            <Preprocess />
      </div>
    )
  }
}

export default Dashboard
