import React, { Component } from 'react'
import { Link, History } from 'react-router'

import auth from 'js/util/auth'

var DashboardHeader = React.createClass({
  mixins: [History],

  logout() {
    auth.logout()
      console.log(this)
    this.history.replaceState(null, '/')
  },

  render() {
    return (
      <div className="header">
          <div className="logo">
        <Link to='/dashboard'>PulseWEB</Link>
              </div>
        <div className="nav-links">
          <a className="nav-logout" onClick={this.logout}><i className="fa fa-sign-out"></i> Log Out</a>
        </div>
      </div>
    )
  }
})

export default DashboardHeader
