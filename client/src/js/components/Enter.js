import React, { Component } from 'react'

import Register from 'js/components/Register'
import Login from 'js/components/Login'

var StatesEnum = {
  REGISTERING: "registering",
  LOGGING_IN: "loggingIn"
}

var Enter = React.createClass({
  getInitialState: function() {
    return {currentState: StatesEnum.REGISTERING};
  },

  promptRegister: function() {
    this.setState({currentState: StatesEnum.REGISTERING});
  },

  promptLogin: function(e) {
    e.preventDefault();
    this.setState({currentState: StatesEnum.LOGGING_IN});
  },

  render: function() {
    var display = null;

    if (this.state.currentState === StatesEnum.REGISTERING) {
      display = <div>
                  <Register />
                  <a onClick={this.promptLogin}>Log in here</a>
                </div>
    } else if (this.state.currentState === StatesEnum.LOGGING_IN) {
      display =
        <div>
          <Login />
          <a onClick={this.promptRegister}>Register here</a>
        </div>
    }

    return display;
  }
});

module.exports = Enter;
