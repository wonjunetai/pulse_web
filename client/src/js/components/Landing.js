import React, { Component } from 'react'

import Enter from 'js/components/Enter'
import LandingHeader from 'js/components/LandingHeader'

class Landing extends Component {
    render() {
        return (
            <div id="landing">
                <LandingHeader />
                <Enter />
            </div>
        )
    }
}

export default Landing
