import React, { Component } from 'react';
import { Link } from 'react-router';

class LandingHeader extends Component {
  render() {
    return (
      <div className="header">
        <div className="logo">
            <Link to='/'>PulseWEB</Link>
        </div>
        <div className="nav-links">
          <Link to="/about" className='nav-about'> About</Link>
        </div>
      </div>
    )
  }
}

export default LandingHeader