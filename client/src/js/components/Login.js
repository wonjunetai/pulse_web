import React from 'react'
import LinkedStateMixin from 'react-addons-linked-state-mixin'
import { connect } from 'react-redux'
import { attemptLogin } from 'js/actions/users'
import auth from 'js/util/auth'
import { History } from 'react-router'

import validator from 'validator'

var Login = React.createClass({
  mixins: [LinkedStateMixin, History],

  getInitialState: function() {
    return {
      email: "",
      password: "",
      submitting: false,
      error: null
    }
  },

  login: function(e) {
    e.preventDefault()
    // grab user credentials
    this.setState({submitting: true}, function() {
      var email = this.state.email
      var password = this.state.password

      this.props.dispatch(attemptLogin(email, password, (status) => {
        if (status) {
          this.history.replaceState(null, '/dashboard')
        } else {
          this.setState({submitting: false, error: "Login failed. Please try again."})
        }
      }))
    })
  },

  render: function() {
    var buttonClass = "pure-button pure-button-primary"
    if (this.state.submitting) {
      buttonClass += " pure-button-disabled"
    }
    return (
        <form className="pure-form pure-form-stacked landing-form" onSubmit={this.login}>
          <p>Login</p>
          <EmailInput emailLink={this.linkState('email')} />
          <PasswordInput passwordLink={this.linkState('password')} />
          <button type="submit" className={buttonClass}>Login</button>
          <p>{this.state.error}</p>
        </form>
    )
  }
})


var EmailInput = React.createClass({
  propTypes: {
    emailLink: React.PropTypes.object.isRequired,
  },

  componentDidMount: function() {
    this.refs.emailLogin.getDOMNode().focus()
  },

  emailChanged: function(e) {
    var email = e.target.value;
    this.props.emailLink.requestChange(email)
  },

  render: function() {
    return (
        <div className="pure-g">
          <input value={this.props.emailLink.value}
                 ref="emailLogin"
                 placeholder="Email"
                 onChange={this.emailChanged}
                 type="text"
                 className="email pure-u-3-5 pure-u-sm-1-3"
          />
        </div>
    );
  }
});

var PasswordInput = React.createClass({
  propTypes: {
    passwordLink: React.PropTypes.object.isRequired,
  },

  passwordChanged: function(e) {
    var password = e.target.value;
    this.props.passwordLink.requestChange(password)
  },

  render: function() {
    return (
        <div className="pure-g">
          <input value={this.props.passwordLink.value}
                 placeholder="Password"
                 onChange={this.passwordChanged}
                 className="password pure-u-3-5 pure-u-sm-1-3"
                 type="password"
          />
        </div>
    )
  }
})

module.exports = connect()(Login)
