import React, { Component } from 'react';

export default class PageNotFound extends Component {
  render() {
    return (
      <div>We couldn't find what you were looking for!</div>
    );
  }
}
