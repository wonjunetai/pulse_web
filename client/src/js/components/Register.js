import React from 'react';
import $ from 'jquery';
import validator from 'validator';
import { connect } from 'react-redux';
import { register, attemptLogin } from 'js/actions/users';
import auth from 'js/util/auth';

import { History } from 'react-router';

var isValidEmail = function(email) {
  return validator.isEmail(email);
}

var isValidPassword = function(password) {
  // get lengths here...
  return validator.isLength(password, 5, 50);
}

var Register = React.createClass({
  mixins: [History],

  getInitialState: function() {
    return {
      email: "",
      emailError: null,
      emailGood: null,
      password: "",
      passwordError: null,
      passwordGood: null,
      submitting: false,
      error: null
    };
  },

  doNothing: function(e) {
    e.preventDefault();
    return false;
  },

  // pressed when user clicks register -- we need to validate.
  register: function(e) {
    if (e) {
      // need this, otherwise form will submit
      e.preventDefault();
    }

  this.validateEmail(null, function() {
    this.validatePassword(null, function() {
      if (this.state.emailError || this.state.passwordError) {
      } else {
        // disable button
        this.setState({submitting: true}, function() {
            var email = this.state.email;
            var pass = this.state.password;

          this.props.dispatch(register(pass, email, (status, data) => {
            if (status) {
              this.props.dispatch(attemptLogin(email, pass, (status) => {
                if (status) {
                  this.history.replaceState(null, '/dashboard');
                } else {
                  this.setState({submitting: false, error: "Login failed. Please try again."});
                }
              }));
            } else {
              this.setState({submitting: false, error: data.body.error});
            }
          }));
        });
      }
    });
  });
  },

  emailChanged: function(e) {
    e.preventDefault();
    var email = e.target.value;
    this.setState({email: email});
  },
  passwordChanged: function(e) {
    e.preventDefault();
    var password = e.target.value;
    this.setState({password: password});
  },

  validateEmail: function(e, callback) {
    if (e) {
      e.preventDefault();
    }

    if (isValidEmail(this.state.email)) {
      this.setState({
        emailGood: "Looks good!",
        emailError: null
      }, callback);
    } else {
      this.setState({
        emailGood: null,
        emailError: "This doesn't look like an email."
      }, callback);
    }
  },

  validatePassword: function(e, callback) {
    if (e) {
      e.preventDefault();
    }
    if (isValidPassword(this.state.password)) {
      this.setState({
        passwordGood: "Great! You're almost there...",
        passwordError: null
      }, callback);
    } else {
      this.setState({
        passwordGood: null,
        passwordError: "Passwords must be at least 5 characters."
      }, callback);
    }
  },

  render: function() {
    var emailError = null;
    if (this.state.emailError) {
      emailError = <div className="error pure-u-3-5 pure-u-sm-1-3">{this.state.emailError}</div>
    }
    var emailInputClass = "email pure-u-3-5 pure-u-sm-1-3";
    if (this.state.emailGood) {
      emailInputClass += " valid";
    }
    var passwordError = null;
    if (this.state.passwordError) {
      passwordError = <div className="error pure-u-3-5 pure-u-sm-1-3">{this.state.passwordError}</div>
    }
    var passwordInputClass = "password pure-u-3-5 pure-u-sm-1-3";
    if (this.state.passwordGood) {
      passwordInputClass += " valid";
    }
    var buttonClassName = "register pure-button pure-button-primary";
    if (this.state.submitting) {
      buttonClassName += " pure-button-disabled";
    }

    return (
        <form className="pure-form pure-form-stacked landing-form" onSubmit={this.register}>
          <p>Create a free account</p>
          <div className="pure-g">
            <input value={this.state.email}
                   ref="emailInput"
                   placeholder="Your email"
                   onChange={this.emailChanged}
                   type="text"
                   className={emailInputClass}
            />
          </div>
          <div className="pure-g">
            {emailError}
          </div>
          <div className="pure-g">
            <input value={this.state.password}
                   placeholder="Make a password"
                   onChange={this.passwordChanged}
                   type="password"
                   className={passwordInputClass}
            />
          </div>
          <div className="pure-g">
            {passwordError}
          </div>
          <button onClick={this.register} type="submit" className={buttonClassName}>Register</button>
          <p>{this.state.error}</p>
        </form>
    );
  }
});

module.exports = connect()(Register);
