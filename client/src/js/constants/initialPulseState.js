import _ from 'lodash'
import initialUserState from 'js/constants/initialUserState'


const initialPulseState = _.merge({},
    initialUserState,
    {
      manifestFile: null
    }
)

export default initialPulseState