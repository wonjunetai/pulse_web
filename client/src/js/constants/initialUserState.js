import { getToken } from 'js/util/auth'

const initialUserState = {
  token: getToken(),
  user: null,
  loading: true
}

export default initialUserState
