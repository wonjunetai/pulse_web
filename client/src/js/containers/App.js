import React, { Component } from 'react'
import { connect } from 'react-redux'
import { History } from 'react-router'

import { fetchUser, noTokenFound } from 'js/actions/users';

var App = React.createClass({
  mixins: [History],

  componentWillMount() {
    if (this.props.token) {
      let intendedPath = this.props.location.pathname

      if (intendedPath === '/') {
        this.props.dispatch(fetchUser(this.props.token,
            this.goToIntendedPathOrLanding('/dashboard')))
      } else {
        this.props.dispatch(fetchUser(this.props.token,
            this.goToIntendedPathOrLanding(intendedPath)))
      }
    } else {
      this.props.dispatch(noTokenFound())
    }
  },

  goToIntendedPathOrLanding(intendedPath) {
    return (status) => {
      if (status) {
        this.history.replaceState(null, intendedPath)
      } else {
        this.history.replaceState(null, '/')
      }
    }
  },

  render() {
    if (this.props.loading) {
      return <i id="spinner" className="fa fa-spinner fa-pulse fa-2x"></i>
    } else {
      return (
          <div>
            {this.props.children}
          </div>
      )
    }
  }
})

App.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  loading: React.PropTypes.bool.isRequired,
  token: React.PropTypes.string.isRequired
}

function select(state) {
  return {
    loading: state.app.loading,
    token: state.app.token
  }
}

export default connect(select)(App)
