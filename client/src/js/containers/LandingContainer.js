import React, { Component } from 'react'
import LandingHeader from 'js/components/LandingHeader'
import Enter from 'js/components/Enter'

class LandingContainer extends Component {
    render() {
        return (
            <div>
                <LandingHeader />
                {this.props.children}
            </div>
        )
    }
}

export default LandingContainer