import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ReduxRouter } from 'redux-router'

import routes from 'js/routes'
import store from 'js/store'

render((
  <Provider store={store}>
    <ReduxRouter routes={routes} />
  </Provider>
), document.getElementById("root"))
