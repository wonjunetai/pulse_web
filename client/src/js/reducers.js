import _ from 'lodash'
import normalize from 'js/util/normalize'

import { RECEIVE_USER,
         NO_TOKEN_FOUND } from 'js/actions/users'

import users from 'js/reducers/users'

import initialPulseState from 'js/constants/initialPulseState'

export default function pulseApp(state = initialPulseState, action) {
  switch (action.type) {
    // Users
    case RECEIVE_USER:
      return users(state, action)
    case NO_TOKEN_FOUND:
      return users(state, action)
    default:
      return state
  }
}
