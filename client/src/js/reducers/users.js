// TODO: Make this into a real reducer
import _ from 'lodash'
import normalize from 'js/util/normalize'

import { RECEIVE_USER,
         NO_TOKEN_FOUND } from 'js/actions/users'

export default function users(state, action) {
  switch (action.type) {
    case RECEIVE_USER:
      // when we receive a user, let's normalize the decks and cards so that
      // instead of them being in an array (and having to lookup their index
      // every time we want to modify them), we have an object prop ids.
      var user = action.user
      return _.assign({}, state, {
        token: action.token,
        loading: false,
        user: user
      })
    case NO_TOKEN_FOUND:
      return _.assign({}, state, {
        token: null,
        loading: false,
        user: null
      })
  }
}
