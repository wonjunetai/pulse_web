import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from 'js/containers/App'
import Landing from 'js/components/Landing'
import LandingContainer from 'js/containers/LandingContainer'
import Dashboard from 'js/components/Dashboard'
import About from 'js/components/About'

import PageNotFound from 'js/components/PageNotFound'

import auth from 'js/util/auth';

let requireAuth = function(nextState, replaceState) {
  if (!auth.loggedIn()) {
    replaceState(null, '/');
  }
};

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Landing} />
        <Route path="dashboard" component={Dashboard} onEnter={requireAuth}>
        </Route>
        <Route component={LandingContainer}>
            <Route path="/about" component={About} />
        </Route>
        <Route path="*" component={PageNotFound} />
    </Route>
)
