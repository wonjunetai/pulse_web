import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { reduxReactRouter, routerStateReducer } from 'redux-router'
import thunkMiddleware from 'redux-thunk'
import createHistory from 'js/util/history'

import pulseApp from 'js/reducers'

const reducer = combineReducers({
  app: pulseApp,
  router: routerStateReducer
})

const store = compose(
  applyMiddleware(thunkMiddleware),
  reduxReactRouter({createHistory}))
(createStore)(reducer)

export default store