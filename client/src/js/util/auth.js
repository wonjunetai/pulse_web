import store from 'store'

module.exports = {
  getToken: function() {
    return store.get('token')
  },

  saveToken: function(token) {
    store.set('token', token)
  },

  loggedIn: function() {
    return !!store.get('token')
  },

  logout: function() {
    store.remove('token')
  }
}
