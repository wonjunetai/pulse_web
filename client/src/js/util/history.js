import _ from 'lodash';
import createHistory from 'history/lib/createHashHistory';

export default function(options) {
  var options = _.assign({}, options, {
    queryKey: false
  });

  return createHistory(options);
};
