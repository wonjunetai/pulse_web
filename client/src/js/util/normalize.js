import _ from 'lodash'

var normalize = function(previousValue, currentValue) {
  var currentObject = {}
  currentObject[currentValue.id] = currentValue
  return _.assign({}, previousValue, currentObject)
}

module.exports = normalize