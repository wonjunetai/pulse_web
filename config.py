import os

IS_PROD = 'IS_PROD' not in os.environ or os.environ['IS_PROD'].lower() != 'false'
DEBUG = not IS_PROD
PORT = int(os.environ.get('PORT', 8000))
DB_HOST = os.environ.get('DB_HOST', 'localhost')
DB_PORT = int(os.environ.get('DB_PORT', 27017))
DB_NAME = os.environ.get('DB_NAME', 'db')
LOG_PATH = os.environ.get('LOG_PATH', '/var/tmp/flaskapp.log')
SECRET_KEY = os.environ.get('SECRET_KEY', 'adcf5ccf83aa2244f3924c6642b62b60f5424593')
ADMINS = ['wonjune.tai@gmail.com']
