import functools

from flask import Blueprint, g
from flask.ext.httpauth import HTTPBasicAuth
from werkzeug.exceptions import Forbidden

from controllers.utils import to_json, add_response_headers
from models.user import User


auth = HTTPBasicAuth()
authapi = Blueprint('auth', __name__)


@authapi.route('/api/token', methods=['POST'])
@add_response_headers({'WWW-Authenticate': 'x-Basic'})
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token()
    return to_json({'token': token.decode('ascii'),
                    'user': g.user.serialize})


@auth.verify_password
def verify_password(email_or_token=None, password=None):
    user = User.verify_auth_token(email_or_token)
    if not user:
        user = User.query.from_email(email_or_token)
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


def auth_required(f):
    @functools.wraps(f)
    def inner(*args, **kwargs):
        uid = kwargs.get('uid')
        if g.user.id != uid:
            raise Forbidden('User {} is forbidden from access'.format(uid))
        return f(*args, **kwargs)
    return auth.login_required(inner)
