from controllers.utils import to_json


BAD_REQUEST_CODE = 400
FORBIDDEN_CODE = 403


def forbidden(error):
    return to_json({'error': str(error),
                    'status': FORBIDDEN_CODE}), FORBIDDEN_CODE


def bad_request(error):
    return to_json({'error': str(error),
                    'status': BAD_REQUEST_CODE}), BAD_REQUEST_CODE
