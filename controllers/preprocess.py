from flask import request, Blueprint, g

from controllers.errors import forbidden
from controllers.utils import to_json

preprocessapi = Blueprint('preprocess', __name__)

@preprocessapi.route('/api/preprocess/')
def upload_manifest():