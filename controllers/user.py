from flask import request, Blueprint, g

from controllers.auth import auth_required, auth
from controllers.errors import bad_request, forbidden, BAD_REQUEST_CODE, FORBIDDEN_CODE
from models.user import EmailAlreadyInUse, User
from controllers.utils import to_json
from models import query
import config


userapi = Blueprint('user', __name__)


@userapi.route('/api/users', methods=['GET'])
@auth.login_required
def get_users():
    assert(g.user.email in config.ADMINS)
    return to_json(query.get_all_users())


@userapi.route("/api/users/<uid>", methods=['GET'])
@auth_required
def get_user(uid):
    return to_json(query.get_user(uid))


@userapi.route('/api/users', methods=['POST'])
def create_user():
    return to_json(query.create_user(request.get_json()))


@userapi.route("/api/users/<uid>", methods=['DELETE'])
@auth_required
def delete_user(uid):
    query.delete_user(uid)
    return to_json(), 204


@userapi.errorhandler(EmailAlreadyInUse)
def email_already_in_use(error):
    return bad_request(error)


@userapi.errorhandler(FORBIDDEN_CODE)
def unauthorized(error):
    return forbidden(error)


@userapi.errorhandler(BAD_REQUEST_CODE)
def bad_value(error):
    return bad_request(error)
