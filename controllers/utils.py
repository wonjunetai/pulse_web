from flask import jsonify, make_response
from datetime import datetime
from functools import wraps


def to_json(model_obj=None):
    if model_obj is None:
        return jsonify()
    elif isinstance(model_obj, list):
        return jsonify(collection=[e.serialize for e in model_obj])
    elif isinstance(model_obj, dict):
        return jsonify(**model_obj)
    else:
        return jsonify(model_obj.serialize)


def epoch(dt):
    return int((dt - datetime(1970, 1, 1)).total_seconds())


def add_response_headers(headers={}):
    """
    This decorator adds the headers passed in to the response

    :param headers:
    :return:
    """
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            resp = make_response(f(*args, **kwargs))
            h = resp.headers
            for header, value in headers.items():
                h[header] = value
            return resp
        return decorated_function
    return decorator
