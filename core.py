from logging.handlers import RotatingFileHandler
import logging

from flask import Flask
import config

app = Flask(__name__, static_folder='client')
logger = app.logger

handler = RotatingFileHandler(filename=config.LOG_PATH)
format = '%(levelname)s %(pathname)s:%(lineno)d %(module)s %(funcName)s %(asctime)s: %(message)s'
handler.setFormatter(logging.Formatter(format))
logger.addHandler(handler)