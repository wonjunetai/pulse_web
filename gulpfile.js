'use strict';

// requirements

var source = require('vinyl-source-stream'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    browserify = require('browserify'),
    size = require('gulp-size'),
    uglify = require('gulp-uglify'),
    concatenify = require('concatenify'),
    babelify = require('babelify'),
    rename = require('gulp-rename'),
    del = require('del'),
    sass = require('gulp-sass'),
    watchify = require('watchify'),
    _ = require('lodash'),
    notifier = require('node-notifier'),
    minifyCss = require('gulp-minify-css'),
    reactify = require('reactify'),
    livereload = require('gulp-livereload');

// tasks
gulp.task('all', ['clean', 'build', 'sass', 'copy']);

gulp.task('clean', function(c) {
    del.sync(['./dist/**/*'], c)
});

var notify = function(error) {
  var message = 'In: ';
  var title = 'Error: ';

  if(error.description) {
    title += error.description
  } else if (error.message) {
    title += error.message
  }

  if(error.filename) {
    var file = error.filename.split('/');
    message += file[file.length-1]
  }

  if(error.lineNumber) {
    message += '\nOn Line: ' + error.lineNumber
  }

  notifier.notify({title: title, message: message})
};

var bundler = watchify(browserify({
  entries: './client/src/js/main.js',
  paths: ['./node_modules', './client/src/'],
  debug: true,
  transform: [babelify.configure({
      presets: ["es2015", "react", "stage-0"]
  }), concatenify],
  cache: {},
  packageCache: {},
  fullPaths: true
}));

function bundle() {
  return bundler
    .bundle()
    .on('error', function(err) {
      console.log(err.toString());
      this.emit('end');
      notify(err)
    })
    .pipe(source('./client/src/js/main.js'))
        .pipe(rename('main.js'))
    .pipe(gulp.dest('./client/dist/js/'))
    .pipe(size())
    .pipe(livereload())
}

bundler.on('update', bundle);

gulp.task('build', function() {
    bundle()
});

gulp.task('compress', function() {
  gulp.src('./client/dist/css/style.css')
    .pipe(minifyCss())
    .pipe(gulp.dest('./client/dist/css/'))

  return gulp.src('./client/dist/js/main.js')
    .pipe(rename('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./client/dist/js/'))
});

gulp.task('watch', function() {
    livereload.listen();
        gulp.watch('./client/src/js/**/*', ['build']);
        gulp.watch('./client/src/jsx/**/*', ['build']);
        gulp.watch('./client/src/models/**/*', ['build']);
        gulp.watch('./client/src/scss/**/*.scss', ['sass']);
        gulp.watch('./client/src/index.html', ['copy']);
        gulp.watch('./client/src/images/**/*', ['copy']);
        gulp.watch('./client/src/css/**/*', ['copy']);
        gulp.watch('./client/src/fonts/**/*', ['copy']);
});

gulp.task('sass', function() {
  gulp.src('client/src/scss/style.scss')
    .pipe(sass({errLogToConsole: true}))
    .pipe(gulp.dest('client/dist/css'))
    .pipe(livereload())
});

gulp.task('copy', function() {
  gulp.src('client/src/index.html')
    .pipe(gulp.dest('client/dist'))
  gulp.src('client/src/css/**/*')
    .pipe(gulp.dest('client/dist/css/'))
  gulp.src('client/src/images/**/*')
    .pipe(gulp.dest('client/dist/images/'))
  gulp.src('client/src/fonts/**/*')
    .pipe(gulp.dest('client/dist/fonts/'))
});

gulp.task('default', ['watch']);
