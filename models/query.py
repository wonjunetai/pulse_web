from datetime import datetime

from bson import ObjectId

from core import logger
from models.user import User


# User
def get_all_users():
    return User.query.all()


def get_user(uid):
    return User.query.get_or_404(uid)


def create_user(user_dict):
    user = User.from_dict(user_dict)
    user.insert()
    return user


def delete_user(uid):
    return User.query.delete_with_uid(uid)
