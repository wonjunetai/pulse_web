from datetime import timedelta
import time

from flask import abort
from flask.ext.mongoalchemy import BaseQuery
from itsdangerous import SignatureExpired, BadSignature, TimedJSONWebSignatureSerializer as Serializer
from mongoalchemy.exceptions import BadValueException
from mongoalchemy.document import Index

from config import SECRET_KEY
from models.model import db
from operations.validation_constants import *
from operations import crypto


TOKEN_EXPIRATION = int(timedelta(days=1).total_seconds())


class UserQuery(BaseQuery):
    def from_email(self, email):
        return self.filter(self.type.email == email).first()

    def email_exists(self, email):
        return bool(self.from_email(email))

    def delete_with_uid(self, uid):
        user = self.query.get(uid)
        if user:
            user.remove()


class User(db.Document):
    query_class = UserQuery

    email = db.StringField(max_length=MAX_EMAIL_LEN)
    password = db.StringField(min_length=MIN_PASSWORD_LEN,
                              max_length=MAX_PASSWORD_LEN)
    created = db.CreatedField()

    Index().ascending('email').unique(drop_dups=True)

    @property
    def id(self):
        return str(self.mongo_id)

    @property
    def created_epoch(self):
        return time.mktime(self.created.timetuple())

    @property
    def serialize(self):
        return {
            'id': self.id,
            'email': self.email,
            'password': self.password,
            'created': self.created_epoch
        }

    @classmethod
    def from_dict(cls, user_dict):
        return cls(email=user_dict.get('email'),
                   password=crypto.hash(user_dict.get('password')))

    def insert(self):
        if self.query.email_exists(self.email):
            raise EmailAlreadyInUse('{} is already in use'.format(self.email))

        super(User, self).save()

    def generate_auth_token(self, secret_key=SECRET_KEY, expiration=TOKEN_EXPIRATION):
        s = Serializer(secret_key, expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token, secret_key=SECRET_KEY):
        s = Serializer(secret_key)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        return User.query.get(data.get('id'))

    def verify_password(self, pw):
        return crypto.verify_pw(pw, self.password)


class UserRegistrationError(Exception):
    pass


class EmailAlreadyInUse(UserRegistrationError):
    pass
