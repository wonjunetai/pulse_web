import bcrypt


def hash(pw):
    return bcrypt.hashpw(pw.encode('utf-8'), bcrypt.gensalt())


def verify_pw(pw, hashed_pw):
    return bcrypt.hashpw(pw.encode('utf-8'), hashed_pw.encode('utf-8')) == hashed_pw
